# Keecker mobile challenge
---------

# 📋 Requirements
The aim of the challenge is to build a simple mobile application.
The code should:
- Be easy to understand.
- Respects seperation of concerns and best practices.
- Usages of 3rd party libraries is allowed but should be justified.
- For Android minimum API is 21, for iOS minimum is iOS 10.
- Quality of versioning and usage of source control system is important.

#  🚀 Delivery
- Gitlab, Github or other hosting platforms.

- We want to create an application that uses Marvel API to list Marvel heroes and informations about them.
[API doc here](https://developer.marvel.com/docs#!/public/getCreatorCollection_get_0)
You should register to get an API key.

- Try to keep UI design basic and simple to use.

- For iOS we prefer that the app is coded in Swift 4.2.
- For Android Kotlin is really nice but not a most.


# ☑️ Must have

- As a user, I want to have a screen that lists Marvel characters.

- As a user, I want to see the details of a Marvel characters on a button tap.
PS: No need to list all available informations.


# 🤩 Bonus

- As a user, I want to be able to add characters to a favorites list.

- As a user, I want to be able to search characters by name.

- Surprise us!
